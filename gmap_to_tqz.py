# python3
# gmap_to_tqz.py
# Tim Littlefair 2019

# See README.md for details of how to use this program

import argparse
import json
import logging
import re
import sys
import unittest

try:
    import requests
    import html5lib
except ModuleNotFoundError as e:
    print()
    print("Module '%s' is not part of the Python Standard Library"%(e.name,))
    print(
        "Install it from the Internet using the command 'python -m pip install %s'" %
        ( e.name, )
    )
    print("You may want to establish a Python 'virtual environment' before you do ")
    print("this to avoid changing the installation for other users or programs.")
    print("See https://docs.python.org/3/tutorial/venv.html")
    print()
    sys.exit(1)  
except ImportError as e:
    print(
        "Error importing module %s, which is  not part of the Python Standard Library"%(
        e.name,)
    )
    print("Your Python installation may contain incompatible non-standard libraries.")
    print("Import error details:")
    raise

# Turn logging on before importing subpackages
logging.basicConfig(level=logging.INFO)

from simple_trail import Trail, generate
from template_tqz import TqzTemplate
from template_preview import PreviewTemplate

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--test",action="store_true")
    parser.add_argument("urls",nargs="*")
    return parser
def _dump_object(name,obj,logging_level):
    if isinstance(obj,list):
        # If the object can be iterated, dump it recursively
        logging.log(logging_level, "%s: length=%d",name,len(obj),)
        for subobj_index in range(0,len(obj)):
            _dump_object(
                "%s[%d]"%(name,subobj_index,),
                obj[subobj_index],
                logging_level
            )
    else:
        logging.log(logging_level,"%s: value=%s",name,obj)

def _extract_trail_attributes(root, attributes):
    attrib_name = None
    attrib_desc = None
    GMM_SUFFIX = " - Google My Maps"

    for element in root.iter():
        if (
            element.tag.endswith("title") and
            element.text is not None and
            element.text.endswith(GMM_SUFFIX)
        ) :
            attrib_name = element.text.replace(GMM_SUFFIX,"")
            logging.info("Extracted name: %s",attrib_name)
        if (
            element.tag.endswith("meta") and
            "itemprop" in element.attrib and
            element.attrib["itemprop"] == "description"
        ):
            attrib_desc = element.attrib["content"]
            logging.info("Extracted description: %s",attrib_desc)
    attributes += [ attrib_name, attrib_desc ]

def _extract_json_pagedata_obj(root):
    # The data we are interested in is all defined in a JSON <script></script>
    # element
    PAGEDATA_DECL_PREFIX = "var _pageData = ";
    PAGEDATA_REGEX = re.compile(
        r"""var _pageData = "(.*)";""",
        flags=re.DOTALL
    )
    pagedata_json = None
    for element in root.iter():
        if (
            element.tag.endswith("script") and
            element.text is not None and
            PAGEDATA_DECL_PREFIX in element.text
        ) :
            logging.debug(element.text)
            match = PAGEDATA_REGEX.search(element.text)
            pagedata_json = match.group(1)
            # De-escape quotes and newlines
            pagedata_json = pagedata_json.replace('\\"','"')
            pagedata_json = pagedata_json.replace('\\n','\n')
            logging.debug(pagedata_json)
            # Convert the JSON object to a Python object
            pagedata_obj = json.loads(pagedata_json)
            _dump_object("pageData",pagedata_obj,logging.DEBUG)
            return pagedata_obj

def _extract_trail_points(obj,points):
    trail_points = []
    if isinstance(obj,list):
        try:
            # By inspection of the output of _dump_object,
            # we know that the substructure for a point 
            # has a URL for a pointer resource at obj[0][0]
            # When we see that this is present, we know that 
            # the name will be at obj[5][0][0] and
            # the coordinates will be at obj[4][4][0:2]
            if "mymaps-pin-container" in obj[0][0]:
                point_data = []
                point_data += [ obj[5][0][0] ]
                point_data += obj[4][4]
                logging.info("Extracted point: %s",point_data)
                points += [ point_data ]
                return
        except TypeError:
            pass
        except IndexError:
            pass
        # if noting was found, recurse
        for subitem in obj:
            _extract_trail_points(subitem,points)

def _extracted_data_to_tqz(name_and_description, points,default_radius=250):
    name, description = name_and_description
    trail = Trail(name, description=description)
    for p in points:
        trail.add_zone( p[0],p[1],p[2],default_radius)
    generate(trail,TqzTemplate())
    generate(trail,PreviewTemplate())

def gmap_url_to_tqz(url, default_radius_metres=250):
    trail_attributes = []
    trail_points = []

    gmap_request = requests.get(url)
    content_string = str(gmap_request.content,"utf-8")
    logging.debug(content_string)
    root = html5lib.parse(content_string)

    # The name and description of the Google Map are extracted from the DOM of the HTML
    _extract_trail_attributes(root,trail_attributes)
 
    # The coordinates and names of points on the Google Map are stored in an
    # object defined in a Javascript <script> element
    pagedata_obj = _extract_json_pagedata_obj(root)
    _extract_trail_points(pagedata_obj,trail_points)
    _extracted_data_to_tqz(trail_attributes,trail_points, default_radius_metres)

# TODO: Clearly this isn't a well structured automated test yet.
def test():
    TEST_GMAP_URLS= {
        "Newcastle upon Tyne, UK": 
            "https://drive.google.com/open?id=1cu2uRnwWB2aqxFVoxOAy1XFjkxlbI3X-&usp=sharing",
        "Perth":
            "https://drive.google.com/open?id=1HbLf8b--laEViWbtH7pDjhO5hY1v50rE&usp=sharing",
        "Gothenburg":
            "https://drive.google.com/open?id=1hRqc_vg8oySGAdZIHYUMkdmYMtodwpPs&usp=sharing",
        "Stockholm":
            "https://drive.google.com/open?id=1y8exDfcBheMcmVNmsi3ZDAuDoKvgkY_4&usp=sharing"
    }
    for url in TEST_GMAP_URLS.values():
        gmap_url_to_tqz(url)

if __name__ == "__main__":
    arg_parser = arg_parser()
    args = arg_parser.parse_args()
    if len(sys.argv) <=1:
        print()
        arg_parser.print_help()
        print()
    elif args.test:
        test()
    else:
        # Additional arguments are assumed to be Google Maps URLs
        for url in args.urls:
            gmap_url_to_tqz(url)


