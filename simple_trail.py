import codecs
import zlib
import os
import re
from collections import namedtuple

from template_tqz import TqzTemplate

def locality_name_to_file_prefix(locality_name):
    # List of reserved characters for FAT32 from 
    # https://en.wikipedia.org/wiki/Filename
    # 0x00-0x1F 0x7F " * / : < > ? \ | + , . ; = [ ] 
    regex = re.compile(r"""[\u0001-\u001f\u007f"\*/:<>\?\\|\+,\.;=\[\]\ ]+""",re.UNICODE)
    locality_name = re.sub(regex,"_",locality_name)
    return locality_name

def synthetic_name(entity_type, lat, lng, radius=None):
    name = "%s centred at %f,%f"%(
        entity_type, lat,lng,
    )
    if radius is not None:
        name += " with radius %d metres" % (radius,)
    return name

class Zone(namedtuple('Zone', 'name, lat, lng, radius, url, address, message, type_info')):
    def __new__(cls, name, lat, lng, radius, url=None, address=None, message=None, type_info=None):
        # We want latitude and longitude to be floats with 6 decimal places
        lat=round(float(lat),6)
        lng=round(float(lng),6)
        # We want the radius to be an integer
        radius=int(radius)
        # The name should not be None, but if it is 
        # we synthesize a name from the latitude longitude and
        # radius
        if name is None:
            name = synthetic_name("Zone",lat,lng,radius)
        # Presently, the trailer iOS app can't handle both a message 
        # and a web reference for the same item, we prefer the 
        # URL if it is available
        if url == "":
            url = None
        if url is not None:
            type_info = message
            message = None
        else:
            type_info = None
        return super(Zone,cls).__new__( cls, name, lat, lng, radius, url, address, message, type_info)

class Trail :
    def __init__(self,locality_name,lat=None,lng=None,description=None):
        self.locality_name = locality_name
        # self.{lat,lng,lat_min,lat_max} are recalculated
        # in add_zone so it does not matter if these
        # are set to an arbitrary value on creation of the 
        # trail
        if lat is None:
            lat = 0.0
        if lng is None:
            lng = 0.0
        self.lat = round(float(lat),3) 
        self.lng = round(float(lng),3)
        self.lat_min, self.lng_min = self.lat,self.lng
        self.lat_max, self.lng_max = self.lat,self.lng
        self.description = description
        self.zones = []
        self.radius_metres = None
        self.recalculate_synthetic_name()

    def recalculate_synthetic_name(self):
        if self.locality_name is None:
            self.locality_name = synthetic_name("Trail",self.lat,self.lng)

    def add_zone(self,name, lat, lng, radius, url=None, address=None, message=None):
        zone = Zone(name, lat, lng, radius, url, address, message)
        self.zones += [ zone ]
        self.lat_min = min(map(lambda z : z.lat,self.zones))
        self.lat_max = max(map(lambda z : z.lat,self.zones))
        self.lng_min = min(map(lambda z : z.lng,self.zones))
        self.lng_max = max(map(lambda z : z.lng,self.zones))
        self.lat = (self.lat_min + self.lat_max) / 2.0
        self.lng = (self.lng_min + self.lng_max) / 2.0
        if self.locality_name.startswith("Trail "):
            self.locality_name = None
            self.recalculate_synthetic_name()

class TrailFactory:

    def create_trail(self, *args, **kwargs):
        return Trail(*args, **kwargs)

    def add_zone_to_trail(self, t, *args, **kwargs):
        t.add_zone(*args, **kwargs)

def scale_zone_radii(t):
    if t.radius_metres is None:
        return
    if len(t.zones)>2 and t.radius_metres>100:
        # Modify the radii of zones to make them large enough
        # but not too large on the trail map.
        for i in range(0,len(t.zones)):
            if t.zones[i].radius < t.radius_metres/30.0:
                t.zones[i] = t.zones[i]._replace(radius = int(t.radius_metres/30.0))
            elif t.zones[i].radius > t.radius_metres/3.0:
                t.zones[i] = t.zones[i]._replace(radius = int(t.radius_metres/3.0))

def generate(t, template, explicit_filename=None,extra_info=None,) :
    scale_zone_radii(t)
    if extra_info is not None :
        extra_info = '-' + extra_info 
    else :
        extra_info = ''
    
    file_prefix = locality_name_to_file_prefix(t.locality_name)
    filename_base = file_prefix + extra_info

    dirpath_and_basename = None
    if explicit_filename is None:
        dirpath_and_basename = filename_base
    elif os.path.isdir(explicit_filename):
        dirpath_and_basename = os.path.join(explicit_filename,filename_base)
    else:
        dirpath_and_basename = explicit_filename
    template.to_file(t,dirpath_and_basename)
                     
if __name__ == "__main__" :
    # One test with only two zones.
    # This means the latitude and longitude 
    # assigned to the trail at the beginning will
    # be preserved
    t1 = Trail("Nowhere special",20.0, 14.6)
    t1.add_zone('A place',21.0,15.0,100)
    t1.add_zone('Another place',20.0, 14.0,200)
    t1.add_zone(None,20.0, 14.0,200)
    generate(t1,TqzTemplate(),"../generated_trails")
        
    # A second test with three zones.
    # The latitude and longitude assigned to 
    # the trail at the beginning will be 
    # overwritten with the means of latitudes
    # and longitudes of zones.
    t2 = Trail("Somewhere else",20.0, 14.6)
    t2.add_zone('First place',21.0,15.0,100)
    t2.add_zone('Second place',20.0, 14.0,200)
    generate(t2,KmlTemplate(),"../generated_trails/SomewhereElse")

    # A third test with a trail and zones which don't
    # have meaningful names
    # This exercises the factory class
    tf = TrailFactory()
    t3 = tf.create_trail(None, 20.0, 14.6)
    tf.add_zone_to_trail(t3, None, 21.0, 15.0, 100)
    tf.add_zone_to_trail(t3, None, 20.0, 14.0, 120)
    tf.add_zone_to_trail(t3, 'Third place',21.0, 14.0,200)
    generate(t3, TqzTemplate(),extra_info="anonymous")
    
        
        