python-trailer-support
======================

Python scripts to support the 'Trailer' mobile applications for iOS 
and Android written by Tim Littlefair.

The Trailer mobile applications are designed to enable viewing map 
on a mobile device with user-created annotations.

The overall Trailer project is very much a hobby project on my own 
part, originally written to 'scratch my own itch' at a time when 
there were fewer options on the web to do similar things.  I am under 
no illusion that it offers any unique advantages in the current 
marketplace, but I continue to work on it as a context in which to 
practice my craft as a programmer and DevOps practitioner. 

What is this package for
------------------------
This package is one of several different attempts I have made over 
time to provide a non-laborious way of creating maps in the XML-based 
.tqz format consumed by the mobile apps.

In this particular attempt, the purpose is to use Google's 'MyMaps' 
service to create a basic page which incorporates the GPS coordinates 
and titles for each location to be annotated, then export this into 
the .tqz format.  Once the .tqz file exists, annotations can be added 
in a text editor and the file can then be sent to a mobile device as 
an email or message attachment for viewing in the Trailer applications.

How can I install this package
------------------------------
In its current form, the only way of installing this package is to clone
the source code repository at 
https://bitbucket.org/tim_littlefair/trailer-gmap-importer.

Note that the repository name and URL are expected to change in the future.


Running the code requires a Python 3 (preferably 3.7) interpreter installed
and on the command line path in the session in the top directory of the 
git sandbox cloned from bitbucket.  
Run the following commands to create a Python virtual environment with the 
right dependencies:

Under Windows 10:
```
python -m venv .venv3
.venv3\scripts\activate.bat
python -m pip --upgrade install pip requests html5lib
```

Under Linux, BSD, MacOS etc:
```
python -m venv .venv3
. .venv3\bin\activate
python -m pip --upgrade install pip requests html5lib
```

The virtual environment only needs to be created and populated with 
dependency libraries once, but the `activate` line needs to be entered
every time a new CLI session in this directory is started.

How can I use this package
-----------------------
The function of this package is to export data from a user-defined 
map created in Google MyMaps service in a format where it can be used
as a starting point for a .tqz map to be consumed by Trailer on a mobile
device.  Before we can start to use the script cloned from bitbucket
we need a Google map to exist.


To create a google map, the user will require a Google account, and a 
browser logged in to that account.  From the logged in browser, enter the URL 
https://mymaps.google.com.  The browser should display a page like this:

![gmaps_screenshot_1][gmaps_screenshot_1]

On this page, click on the red 'Create a new map' button, 

![gmaps_screenshot_2][gmaps_screenshot_2]

A map will appear.  When I do this I see a map scaled to display continents, 
centred on my location (Western Australia).

On this map, in the search box, enter the name of one of the places you want 
to appear on your user-generated map (using Google's autocomplete suggestions 
if you want).  

![gmaps_screenshot_3][gmaps_screenshot_3]

When you select an autocomplete option or press the search 
button to complete the search, your map should zoom in to the place you've 
specified (or maybe to different possible candidate locations if the search 
returns multiple results).

![gmaps_screenshot_4a][gmaps_screenshot_4a]

On this screen, select one of the green markers in the search results area 
on the left hand side to pop up the annotation box describing a search 
result (if the search only had one result it will already be selected and 
the annotation box will be popped up).  

Once the annotation is popped up, click on the 'Add to map' button and the 
item will be added as a marker to the map.

You can continue to add additional items to the map using the search box, 
and can pop up and edit the title for any given item until the places you 
want have been added.  For example, Google's autocomplete suggestions
gave me a marker which was titled 'Government House', but I wanted
it to be titled 'Government House Gardens':

![gmaps_screenshot_5a][gmaps_screenshot_5a]

Once the map has all of the features you want, add a title, and click on the 'Share' 
button to make it available without logging in.

![gmaps_screenshot_6a][gmaps_screenshot_6a]

![gmaps_screenshot_6b][gmaps_screenshot_6b]

Copy the URL from these screens and you finally have something you can process
using the installed clone of this code:

```
python .\gmap_to_tqz.py "https://drive.google.com/open?id=1HbLf8b--laEViWbtH7pDjhO5hY1v50rE&usp=sharing"
WARNING:root:HTML preview generation disabled because no Mapbox API token found
INFO:root:Extracted name: Perth, Western Australia
INFO:root:Extracted description: Places to visit on lunchtime walks, live music and theatre venues for the evening.
INFO:root:Extracted point: ['Government House Gardens', -31.95709490000001, 115.86271770000008]
INFO:root:Extracted point: ['Queens Gardens', -31.9593877, 115.87684760000002]
INFO:root:Extracted point: ['The Ellington Jazz Club', -31.9466589, 115.86428909999995]
INFO:root:Extracted point: ['Rosemount Hotel', -31.9308165, 115.85905920000005]
INFO:root:Extracted point: ['Toffee Cafe', -31.9545598, 115.86104120000005]
INFO:root:Extracted point: ["His Majesty's Theatre", -31.9528674, 115.85458719999997]
INFO:root:Extracted point: ['The Astor Theatre', -31.934086, 115.87205900000004]
INFO:root:Extracted point: ['The Sewing Room Perth', -31.952257, 115.85601099999997]
INFO:root:Trail serialized to file: Perth_Western_Australia.tqz
```

The file generated will look like [this][generated_tqz].  The intention is 
that this file can then be edited using a text editor, for example to give
something like [this][user_edited_tqz].

Once the .tqz file has been edited by hand and contains all of the details
you want it to, you can send it as an email attachment to the device (yours
or someone else's) where you want it to be viewed.  It should also be possible
to send it as a link to a copy posted on the web, or an attachment via
a messaging app.  Clicking on the attahchment in the iOS or Android app
should load the file and allow it to be viewed.

Where to buy the Trailer mobile applications for iOS and Android:
-----------------------------------------------------------------
<table>
<tbody>
<td>
<a href="https://itunes.apple.com/us/app/tl-trailer/id511877626?mt=8" style="display:inline-block;overflow:hidden;background:url(https://linkmaker.itunes.apple.com/en-us/badge-lrg.svg?releaseDate=2012-07-27&kind=iossoftware&bubble=ios_apps) no-repeat;width:135px;height:40px;"></a>
</td>
<td width="10px">
&nbsp;
</td>
<td>
<a href="https://play.google.com/store/apps/details?id=com.blogspot.tltrailer.androidapp">
  <img alt="Get it on Google Play"
       src="https://developer.android.com/images/brand/en_generic_rgb_wo_45.png" />
</a>
</td>
</tr>
</tbody>
</table>


[gmaps_screenshot_1]: ./docs/images/gmaps_screenshot_1.jpg
[gmaps_screenshot_2]: ./docs/images/gmaps_screenshot_2.jpg
[gmaps_screenshot_3]: ./docs/images/gmaps_screenshot_3.jpg
[gmaps_screenshot_4]: ./docs/images/gmaps_screenshot_4a.jpg
[gmaps_screenshot_5a]: ./docs/images/gmaps_screenshot_5a.jpg
[gmaps_screenshot_6a]: ./docs/images/gmaps_screenshot_6a.jpg
[gmaps_screenshot_6b]: ./docs/images/gmaps_screenshot_6b.jpg
[generated_tqz]: ./docs/Perth_Western_Australia-generated.tqz
[user_edited_tqz]: ./docs/Perth_Western_Australia-edited-by-user.tqz
