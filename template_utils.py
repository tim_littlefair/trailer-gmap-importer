# template_utils.py

XML_AMPERSAND_ESCAPE = { "&":  "&amp;" }
XML_OTHER_ESCAPES = { 
    '"':  "&quot;",
    "'":  "&apos;",
    "<":  "&lt;",
    ">":  "&gt;",
}

def sanitize(insanitary_string):
    if insanitary_string is None:
        return ""
    else:
        string = insanitary_string
        for k in XML_AMPERSAND_ESCAPE.keys():
            string = string.replace(k,XML_AMPERSAND_ESCAPE[k])
        for k in XML_OTHER_ESCAPES.keys():
            string = string.replace(k,XML_OTHER_ESCAPES[k])
        return string

def render_value(value):
    if isinstance(value,float):
        return "%.6f"%(value)
    else:
        return sanitize(str(value))

def add_attr_line(existing_value,prefix,attr_name,attr_value) :
    if attr_value is None :
        return existing_value
    else :
        extra_item = prefix + attr_name + '="' + render_value(attr_value) + u'"\n'
        if existing_value is None :
            return extra_item
        else :
            return existing_value + extra_item

