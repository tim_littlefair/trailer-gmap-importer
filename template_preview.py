# python3
# template_preview.py
# Script to convert a trail object into an HTML preview page
import logging
import os

from template_utils import add_attr_line

access_token = None

try:
    token_filename = os.path.expanduser("~/mapbox-api-token.txt")
    access_token = open(token_filename,"rt").read().strip()
except FileNotFoundError:
    logging.warning("HTML preview generation disabled because no Mapbox API token found")

class PreviewTemplate:
    def __init__(self,filename="trail_template.html"):
        self.template_text = open(filename,"rt").read()
        pass
        
    def to_bytes(self,t):
        if access_token is None:
            return
        zone_lines = []
        for z in t.zones :
            zone_line = '    addZone(mapView, %f, %f, %d, "%s");' %(
                z.lat, z.lng, 
                z.radius,
                z.name
            )
            zone_lines += [ zone_line ]
        preview_text = self.template_text.replace("!access_token!",access_token)
        preview_text = preview_text.replace("!add_zone_lines!","\n".join(zone_lines))
        return bytes(preview_text,'utf-8')

    def to_file(self, t, dirpath_and_basename):
        if access_token is None:
            return
        fname = dirpath_and_basename + ".html"
        f = open( fname, mode='wb' )
        f.write(self.to_bytes(t))
        f.close()
        logging.info("Trail serialized to file: %s",fname)
        
