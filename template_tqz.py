# python3
# template_tqz.py
# Script to convert a trail object into the XML-based TQZ format
# consumed by the tl-trailer mobile apps.

import logging

from template_utils import add_attr_line

_TRAIL_XML_TEMPLATE = """<?xml version="1.0" encoding="utf-8"?>
<tw1>
    <tw1_plaintext 
%s
    >
%s
    </tw1_plaintext>
</tw1>
"""

_TRAIL_ATTR_PREFIX = " "*8

_ZONE_XML_TEMPLATE = """        <zone
%s
        />
"""

_ZONE_ATTR_PREFIX = " "*12

class TqzTemplate:
    def __init__(self):
        pass
        
    def to_bytes(self,t):
        zone_node_list = ""
        for z in t.zones :
            zone_attr_list = ""
            zone_attr_list = add_attr_line(zone_attr_list,_ZONE_ATTR_PREFIX,"name",z.name)
            zone_attr_list = add_attr_line(zone_attr_list,_ZONE_ATTR_PREFIX,"latitude",z.lat)
            zone_attr_list = add_attr_line(zone_attr_list,_ZONE_ATTR_PREFIX,"longitude",z.lng)
            zone_attr_list = add_attr_line(zone_attr_list,_ZONE_ATTR_PREFIX,"radius_metres",z.radius)
            zone_attr_list = add_attr_line(zone_attr_list,_ZONE_ATTR_PREFIX,"address",z.address)
            zone_attr_list = add_attr_line(zone_attr_list,_ZONE_ATTR_PREFIX,"message",z.message)
            zone_attr_list = add_attr_line(zone_attr_list,_ZONE_ATTR_PREFIX,"url",z.url)
            zone_attr_list = add_attr_line(zone_attr_list,_ZONE_ATTR_PREFIX,"type_info",z.type_info)
            zone_node_list += _ZONE_XML_TEMPLATE % ( zone_attr_list.rstrip() )
        trail_attr_list = ""
        trail_attr_list = add_attr_line(trail_attr_list,_TRAIL_ATTR_PREFIX,"locality",t.locality_name)
        trail_attr_list = add_attr_line(trail_attr_list,_TRAIL_ATTR_PREFIX,"latitude",t.lat)
        trail_attr_list = add_attr_line(trail_attr_list,_TRAIL_ATTR_PREFIX,"longitude",t.lng)
        trail_attr_list = add_attr_line(trail_attr_list,_TRAIL_ATTR_PREFIX,"description",t.description)
        trail_attr_list = add_attr_line(trail_attr_list,_TRAIL_ATTR_PREFIX,"mode","1")
        trail_attr_list = add_attr_line(trail_attr_list,_TRAIL_ATTR_PREFIX,"version","1")
        trail_tqz_string = _TRAIL_XML_TEMPLATE % ( trail_attr_list.rstrip(), zone_node_list.rstrip() )
        return bytes(trail_tqz_string,'utf-8')

    def to_file(self, t, dirpath_and_basename):
        fname = dirpath_and_basename + ".tqz"
        f = open( fname, mode='wb' )
        f.write(self.to_bytes(t))
        f.close()
        logging.info("Trail serialized to file: %s",fname)
        
